import fetch from 'isomorphic-fetch'

export const LOAD_DOVES = 'LOAD_DOVES'
export const LOAD_COMMANDS = 'LOAD_COMMANDS'
export const OPEN_DIALOG = 'OPEN_DIALOG'

export const openDialog = (open) => {
	return {
		type: OPEN_DIALOG,
		open
	}
}

export const loadCommands = (json) => {
	return {
		type: LOAD_COMMANDS,
		doves: json
	}
}

export const loadDoves = (json) => {
	return {
		type: LOAD_DOVES,
		doves: json
	}
}

export const fetchDoves = () => dispatch => {
	return fetch(`http://localhost:3000/doves`)
		.then(response => response.json())
		.then(json => {
			dispatch(loadCommands(json))
			dispatch(loadDoves(json))
		})
}

export const searchDoves = filter => dispatch => {
	let queryStr = ''
	if(filter.active === 'true') {
		queryStr += '&active=true'
	}else if(filter.active === 'false') {
		queryStr += '&active=false'
	}

	if(filter.command !== '') {
		queryStr += `&last_command=${filter.command}`
	}

	return fetch(`http://localhost:3000/doves?${queryStr}`)
		.then(response => response.json())
		.then(json => dispatch(loadDoves(json)))
}

export const addDove = (dove) => dispatch => {

	return fetch(`http://localhost:3000/doves?`, {
			method: 'POST',
			headers: {
			    'Accept': 'application/json, text/plain, */*',
			    'Content-Type': 'application/json'
			},
			body: JSON.stringify(dove)
		})
		.then(response => {
			if(response.status===201) {
				dispatch(openDialog(false))
				dispatch(fetchDoves())
			}
		})
}
