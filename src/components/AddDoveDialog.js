import React, { Component } from 'react'
import { connect } from 'react-redux'
import { addDove, openDialog } from '../actions'
import Dialog from 'material-ui/Dialog'
import FlatButton from 'material-ui/FlatButton'
import { BlockPicker } from 'react-color'
import DropDownMenu from 'material-ui/DropDownMenu'
import MenuItem from 'material-ui/MenuItem'
import TextField from 'material-ui/TextField'
import moment from 'moment'

import './AddDoveDialog.css'

class AddDoveDialog extends Component {
  constructor(props) {
    super(props) 

    this.state = {
      active: 'true',
      images_collected: '',
      command: '',
      color: "#FF6900"
    }
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      command: nextProps.commands[0]
    })
  }

  handleActiveChange = (event, index, value) => {
    this.setState({
      active: value
    })
  }

  handleCommandChange = (event, index, value) => {
    this.setState({
      command: value
    })
  }

  handleImagesCollectedChange = event => {
    this.setState({
      images_collected: event.target.value
    })
  }

  handleAdd = (event) => {
    const timestamp = moment().format()
    this.props.dispatch(
      addDove({
        active: this.state.active,
        last_command: this.state.command,
        images_collected: this.state.images_collected || 0,
        color: this.state.color,
        deorbit_dt: timestamp
      })
    )
  }

  handleCloseDialog = () => {
    this.props.dispatch(openDialog(false))
  }

  handleColorChange = (color, event) => {
    this.setState({ color: color.hex })
  }

  render() {
    const actions = [
        <FlatButton
          label="Cancel"
          primary={true}
          onTouchTap={this.handleCloseDialog}
        />,
        <FlatButton
          label="Add"
          primary={true}
          onTouchTap={this.handleAdd}
        />,
      ]

    return (
      <Dialog
          title="Add new dove"
          actions={actions}
          modal={true}
          open={this.props.openDialog}
        >
          <div className="dialog" onSubmit={this.handleAdd}>
          <DropDownMenu
            style={{ width: '150px' }}
            autoWidth={false}
            value={this.state.active} 
            onChange={this.handleActiveChange}
          >
            <MenuItem value='true' primaryText='true' />
            <MenuItem value='false' primaryText='false' />
          </DropDownMenu> 
          <DropDownMenu 
            style={{ width: '300px' }}
            autoWidth={false}
            value={this.state.command} 
            onChange={this.handleCommandChange}
          >
            { this.props.commands.map( command => {
                return <MenuItem key={command} value={command} primaryText={command} />
            })}
          </DropDownMenu>
          <TextField 
            className="dialog-item"
            hintText="Images Collected" 
            onChange={this.handleImagesCollectedChange} 
          />
          <div className="dialog-item">
            <BlockPicker 
              width="300px"
              triangle="hide"
              color={ this.state.color }
              onChangeComplete={ this.handleColorChange }
            />
          </div>
        </div>
      </Dialog>
    )
  }
}

function mapStateToProps( state ) {
  return {
    doves: state.doves || [],
    commands: state.commands || [],
    openDialog: state.openDialog
  }
}

export default connect(mapStateToProps)(AddDoveDialog)