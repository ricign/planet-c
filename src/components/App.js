import React, { Component } from 'react'
import { connect } from 'react-redux'
import FilterForm from './FilterForm'
import AddDoveDialog from './AddDoveDialog'
import DoveList from './DoveList'
import './App.css';


class App extends Component {

  render() {
    return (
      <div className="App">
        <div className="header">
          <h2>Planet Chalenge</h2>
        </div>
        <div>
          <AddDoveDialog 
            onClose={this.handleDialog} 
          />
          <FilterForm />
          <DoveList 
            doves={this.props.doves} 
          />
        </div>
      </div>
    );
  }
}

function mapStateToProps( state ) {
  return {
    doves: state.doves || [],
    openDialog: state.openDialog
  }
}

export default connect(mapStateToProps)(App)