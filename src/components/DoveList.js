import React from 'react'
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table'

const DoveList = ({
	doves
}) => (
	<Table>
    <TableHeader
    	adjustForCheckbox={false}
    	displaySelectAll={false}
    >
      <TableRow>
        <TableHeaderColumn>ID</TableHeaderColumn>
        <TableHeaderColumn>Active</TableHeaderColumn>
        <TableHeaderColumn>Color</TableHeaderColumn>
        <TableHeaderColumn>Images Collected</TableHeaderColumn>
        <TableHeaderColumn>Last Command</TableHeaderColumn>
        <TableHeaderColumn>Deorbit Dt</TableHeaderColumn>
      </TableRow>
    </TableHeader>
    <TableBody
    	displayRowCheckbox={false}
    >
    {  doves.map( dove => {
      return  <TableRow key={dove.id}>
  			         <TableRowColumn>{dove.id}</TableRowColumn>
  			         <TableRowColumn>{dove.active.toString()}</TableRowColumn>
  			         <TableRowColumn style={{backgroundColor: dove.color}}>{dove.color}</TableRowColumn>
  			         <TableRowColumn>{dove.images_collected}</TableRowColumn>
  			         <TableRowColumn>{dove.last_command}</TableRowColumn>
  			         <TableRowColumn>{dove.deorbit_dt}</TableRowColumn>
  			      </TableRow>

    })}
	  </TableBody>
  </Table>
)

export default DoveList