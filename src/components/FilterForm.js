import React, { Component } from 'react'
import { connect } from 'react-redux'
import { fetchDoves, searchDoves, openDialog } from '../actions'
import DropDownMenu from 'material-ui/DropDownMenu'
import MenuItem from 'material-ui/MenuItem'
import RaisedButton from 'material-ui/RaisedButton'
import './FilterForm.css'

class FilterForm extends Component {
  constructor(props) {
    super(props) 

    this.state = {
      active: '',
      command: '',
    }
  }

  componentDidMount() {
    this.props.dispatch(fetchDoves())
  }

  handleActiveChange = (event, index, value) => {
    this.setState({
      active: value
    })
  }

  handleCommandChange = (event, index, value) => {
    this.setState({
      command: value
    })
  }

  handleSubmit = event => {
    event.preventDefault()
    this.props.dispatch(
      searchDoves({
        active: this.state.active,
        command: this.state.command
      })
    )
  }

  handleDialog = () => {
    this.props.dispatch(openDialog(true))
  }

	render() {
		return(
			<div className="filter"> 
        <DropDownMenu
          anchorOrigin={{vertical: 'bottom', horizontal: 'left'}} 
          style={{ width: '150px' }}
          autoWidth={false}
          value={this.state.active} 
          onChange={this.handleActiveChange}
        >
          <MenuItem value='' primaryText='' />
          <MenuItem value='true' primaryText='true' />
          <MenuItem value='false' primaryText='false' />
        </DropDownMenu> 
        <DropDownMenu 
          anchorOrigin={{vertical: 'bottom', horizontal: 'left'}} 
          style={{ width: '300px' }}
          autoWidth={false}
          value={this.state.command} 
          onChange={this.handleCommandChange}
        >
          <MenuItem value='' primaryText='' />
          { this.props.commands.map( command => {
              return <MenuItem key={command} value={command} primaryText={command} />
          })}
        </DropDownMenu>
        <RaisedButton
          className="button"
          label="Search" 
          primary={true} 
          onClick={this.handleSubmit}
        />
        <RaisedButton
          className="button"
          label="Add New" 
          primary={true} 
          onClick={this.handleDialog}
        />
      </div>
		)
	}
}

function mapStateToProps( state ) {
  return {
    doves: state.doves || [],
    commands: state.commands || []
  }
}

export default connect(mapStateToProps)(FilterForm)