import { combineReducers } from 'redux'

import {
	LOAD_DOVES, 
	LOAD_COMMANDS,
	OPEN_DIALOG
} from '../actions'

const doves = ( state = [], action ) => {
	switch( action.type ) {
		case LOAD_DOVES:
			return action.doves
		default:
			return state
	}
}

const commands = ( state = [], action ) => {
	switch( action.type ) {
		case LOAD_COMMANDS:
			return action.doves.reduce( (result, dove) => {
				return result.includes(dove.last_command) ? 
					result : 
					result.concat(dove.last_command)
			}, []).sort()
		default:
			return state 
	}
}

const openDialog = ( state = false, action ) => {
	switch( action.type ) {
		case OPEN_DIALOG:
			return action.open
		default:
			return state
	}
}

const rootReducer = combineReducers({
	doves,
	commands,
	openDialog
})

export default rootReducer